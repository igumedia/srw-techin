// i dont fully understand the requirement (what is the input and expected output)
// so I assume we need a function to check wheter a word is palindrom or not
function isPalindrome(sentence){

    let convertedToLowerString = sentence.toLowerCase().replace(/[\W_]/g, '');
    let reverseStringOrder = convertedToLowerString.split('').reverse().join(''); 
    return reverseStringOrder === convertedToLowerString;
}

module.exports = isPalindrome