const sortArray = require("../sortArray");
const symmetricDifference = require("../symmetricDifference");
const isPalindrome = require('../isPalindrome');

test("Sort array in increasing order of [3,4,5,1,2] is [1,2,3,4,5] ", () => {
  expect(sortArray([3, 4, 5, 1, 2])).toEqual([1, 2, 3, 4, 5]);
});

test("Enumerates the items that are in A or B but not both.", () => {
  expect(symmetricDifference([1, 2, 3, 4, 5], [3, 4, 8, 7])).toEqual([
    1,
    2,
    5,
    7,
    8,
  ]);
});

test("Check whether a the string polindrome or not", () => {
  expect(isPalindrome("A man, a plan, a canal, Panama!")).toBe(true)
});
