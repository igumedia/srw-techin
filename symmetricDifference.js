function symmetricDifference(a, b) {

  let c = [];

  let d = [];

  a.forEach((item) => {
    if (!b.includes(item)) {
      c.push(item);
    }
  });

  b.forEach((item) => {
    if (!a.includes(item)) {
      d.push(item);
    }
  });

  return [...c, ...d].sort((a,b)=> a-b);
}

module.exports = symmetricDifference;
